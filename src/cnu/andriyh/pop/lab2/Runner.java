package cnu.andriyh.pop.lab2;

import java.util.function.Consumer;
import java.util.function.Function;

public class Runner implements Runnable {

    private final int id;
    private final int left;

    private final int right;
    private final Function<Integer, Integer> arrayAccess;
    private final Consumer<Integer> globalMin;


    public Runner(int id, int left, int right, Function<Integer, Integer> arrayAccess, Consumer<Integer> globalMin) {
        this.id = id;
        this.left = left;
        this.right = right;
        this.arrayAccess = arrayAccess;
        this.globalMin = globalMin;
    }

    @Override
    public void run() {
        int min = Integer.MAX_VALUE;
        for (int i = left; i < right; i++) {
            min = Math.min(min, arrayAccess.apply(i));
        }
        globalMin.accept(min);
        System.out.println("Потік: [" + id + "] пройшов відрізок [" + left + " - " + (right-1) + "] та знайшов мінімум: " + min);
    }
}
