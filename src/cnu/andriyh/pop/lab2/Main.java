package cnu.andriyh.pop.lab2;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Відсутній аргумент програми - кількість потоків");
            return;
        }
        int threads = Integer.parseInt(args[0]);

        try {
            int min = new ConcurrentArray(10_000_000, threads).calculateMin();
            System.out.println("Глобальний мінімум: " + min);
        } catch (InterruptedException e) {
            System.out.println("Основний потік перервано!");
            Thread.currentThread().interrupt();
        }
    }
}
