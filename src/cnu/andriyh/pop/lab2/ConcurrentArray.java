package cnu.andriyh.pop.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ConcurrentArray {
    private final int size;
    private final List<Runner> runners;

    private final int[] _array;

    private volatile int min;

    public ConcurrentArray(int size, int threads) {
        this.size = size;
        this.runners = createThreads(threads);
        _array = new int[size];
        initArray();
    }

    private void initArray() {
        for (int i = 0; i < size; i++) {
            _array[i] = i;
        }
        Random random = new Random();
        int secret = random.nextInt(size);
        _array[secret] = -1;
        System.out.println("Згенеровано масив з -1 за індексом: " + secret);
    }

    public int get(int index) {
        return _array[index];
    }

    public int calculateMin() throws InterruptedException {
        for (int i = 0; i < runners.size(); i++) {
            Thread thread = new Thread(runners.get(i));
            thread.start();
            thread.join();
        }
        return min;
    }

    private synchronized void consumeMin(int min) {
        this.min = Math.min(min, this.min);
    }

    private List<Runner> createThreads(int threads) {
        List<Runner> runners = new ArrayList<>();
        int left = 0;
        int right = size / threads;
        for (int i = 0; i < threads; i++) {
            runners.add(new Runner(i, left, right, this::get, this::consumeMin));
            left = right;
            right = Math.min(size, right + size / threads);
        }
        return runners;
    }
}
